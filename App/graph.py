from collections import defaultdict


class Graph(object):


	def __init__(self, vertices=None):
		self._graph = {}
		if vertices:
			self.add(vertices)


	# Adiciona vertices no grafo
	def add(self, vertices):
		for node1, node2, weight in vertices:
			if node1 not in self._graph:
				self._graph.update({node1:[[node2,weight]]})
			else:
				self._graph[node1].append([node2, weight])


	# Retorna o peso do vertice
	def weight(self, node1, node2):
		for node in self._graph[node1]:
			if node2 in node:
				return node[1]


	# Verifica se está conectado
	def is_connected(self, node1, node2):
		for node in self._graph[node1]:
			return node2 in node
		
	def __str__(self):
		return '{}({})'.format(self.__class__.__name__, dict(self._graph))

