class ResultObj(object):

	def __init__(self, way=[], weight=[]):
		self._way = way
		self._weight = weight


	def total(self):
		return sum([int(w) for w in self._weight])

	def way(self):
		return self._way


class Search(object):

	def dfs(self, file_input):
		self._g = file_input.graph._graph
		self._final = file_input.final
		self._start = file_input.start

		visited = [self._start]
		stack 	= [self._start]
		total_weight = ['0']

		comp = 0
		atrib = 0

		while stack:
			# Cabeça da pilha
			node = stack[-1]
			atrib = atrib+1

			comp = comp + 1
			# Se o no escolhido tem filho
			if node in self._g:

				i = 0
				atrib = atrib+1
				# Roda os filhos dele
				for node_son, weight in self._g[node]:
					i = i + 1
					atrib = atrib+3

					comp = comp + 1					
					# Se o filho ainda n foi visitado, visita e add na pilha
					if node_son not in visited:
						visited.append(node_son)
						stack.append(node_son)
						total_weight.append(weight)
						atrib = atrib+3
						# Se for o final, já retorna a pilha
						comp = comp + 1

						if node_son == self._final:

							print(atrib)
							print(comp)
							return ResultObj(stack, total_weight)
					
						break

					# Não tem mais ngm pra visitar, desempilha
					if i == len(self._g[node]):
						comp = comp + 1
						stack.pop()
						total_weight.pop()

			# Não tem filho, desempilha
			else:
				stack.pop()
				total_weight.pop()

		# retorna a pilha vazia pq n achou ngm
		return None


	def greedy(self, file_input):
		self._h = file_input.h
		self._g = file_input.graph._graph
		self._final = file_input.final
		self._start = file_input.start

		stack   = [self._start]
		visited = []
		total_weight = ['0']

		comp = 0
		atrib = 0

		while stack:
			# Remove da frente da fila
			node = stack[-1]
			atrib = atrib + 3

			small = None
			i = 0

			comp = comp + 1
			# Se o no escolhido tem filho
			if node in self._g:

				small_node = self._g[node][0][0]
				small_weight = float('inf')

				atrib = atrib + 2

				# Le os filhos dele
				for node_son, weight in self._g[node]:
					i = i + 1
					atrib = atrib + 3
					comp = comp + 1
					if node_son == self._final:
						stack.append(node_son)
						total_weight.append(weight)
						atrib = atrib + 3
						print(atrib)
						print(comp)
						return ResultObj(stack, total_weight)

					comp = comp+1
					# Se esse filho não foi visitado
					if node_son not in visited:
						# Se esse filho tiver heuristica para o fim
						comp = comp + 1
						if node_son in self._h:
							h_son = self._h[node_son][0][1]
							atrib = atrb + 1
							# Verifica se essa é o menor caminho encontrado até o momento, 
							# se sim, atribui ao no small_node o melhor caminho
							comp = comp + 1
							if float(h_son) < float(small_weight):
								small_node   = node_son
								small_weight = weight
								atrib = atrib + 1

					comp = comp + 1
					# Visitou todos os filhos possíveis ou nenhum
					if i == len(self._g[node]):

						comp = comp + 1
						# Se o valor para o menor mudou, significa que há um caminho a percorrer
						if small_weight != float("inf"):
							# diz que o filho foi visitado
							visited.append(small_node)
							# adiciona na pilha o filho
							stack.append(small_node)
							
							total_weight.append(weight)
							atrib = atrib + 3
						
						# Se não volta para o nó anterior
						else:
							# remove o pai da pilha pq ele n tem mais filhos não-vistados
							node = stack.pop()
							total_weight.pop()


			# O nó escolhido n tem filho, coloca seu pai de volta na pilha 
			else:
				stack.pop()
				total_weight.pop()

		return stack









	# def astar(self, file_input):
	# 	self._h = file_input.h
	# 	self._g = file_input.graph._graph
	# 	self._final = file_input.final
	# 	self._start = file_input.start

	# 	nodes_open   = [self._start]
	# 	nodes_closed = []


	# 	while nodes_open:

	# 		# Remove da frente da fila
	# 		node = nodes_open.pop()
	# 		small = None
	# 		i = 0

	# 		# Se o no escolhido tem filho
	# 		if node in self._g:

	# 			small_node = self._g[node][0][0]
	# 			small_weight = float('inf')

	# 			# Roda os filhos dele
	# 			for node_son, weight in self._g[node]:
	# 				i = i + 1

	# 				# Se esse filho tiver heuristica para o fim
	# 				if node_son in self._h:


	# 					# Soma a heuristica com o custo para chegar até ele
	# 					node_heuristic = float(self._h[node_son][0][1]) + float(weight)
					
	# 					# Verifica se essa é o menor caminho encontrado até o momento, 
	# 					# se sim, atribui ao no small_node o melhor caminho
	# 					if node_heuristic < float(small_weight):
	# 						small_node   = node_son
	# 						small_weight = node_heuristic
	# 						print(node + small_node)


	# 				# Visitou todos os nós, add o menor na fila de abertos
	# 				if i == len(self._g[node]):
	# 					nodes_closed.append(node)
	# 					nodes_open.append(small_node)

	# 	return nodes_closed


