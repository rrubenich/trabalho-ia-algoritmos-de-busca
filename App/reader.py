import string
import re

from graph import Graph

class FileInput:

	def __init__(self, start, final, graph, h=None):
		self.start = start
		self.final = final 
		self.graph = graph
		self.h 	   = h


class ReadInput:

	def open(self, file_to_read):
		try:
			# file  = open(file_to_read, 'r', encoding='latin1')
			file  = open(file_to_read, 'r')
			lines = file.readlines()
			file.close()
			return lines
		except:
			print("Arquivo não existe")


	def create_dict(self, itens, dict_itens):
		itens = itens.replace(").\n","").split(",")
		key   = itens[0]

		if not key in dict_itens.keys():
			dict_itens.update({key:[[itens[1],itens[2]]]})
		else:
			dict_itens[key].append([itens[1],itens[2]])


	def add_graph(self, line, g):
		nodes  = line.replace(").\n","").split(',')
		g.add([(nodes[0],nodes[1],nodes[2])])


	def read(self, lines):

		start = None
		final = None
		g = Graph()
		h = {}

		while(lines):
			line = lines[0].split("(")

			if line[0] is not '\n':
				if line[0] == 'início':
					start = line[1].replace(").\n","")

				elif line[0] == 'final':
					final = line[1].replace(").\n","")

				elif line[0] == 'caminho':
					self.add_graph(line[1], g)

				elif line[0] == 'h':
					self.create_dict(line[1], h)

			del(lines[0])

		return FileInput(start, final, g, h)