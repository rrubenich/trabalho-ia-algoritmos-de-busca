def main():
	from reader import ReadInput
	from graph import Graph
	from search import Search
	import os

	try:
		os.system('cls')	
		os.system('clear')
	except:
		pass

	file_to_read = input("Caminho do arquivo: ")

	while not os.path.exists(file_to_read):
		print("Caminho inválido!")
		file_to_read = input("Caminho do arquivo: ")

	reader = ReadInput()
	lines = reader.open(file_to_read)
	file_input = reader.read(lines)

	search = Search()
	

	menu = True	
	while menu:

		print("\nBuscas:")
		print("1 - Busca em Profundidade")
		print("2 - Busca Gulosa")
		search_type = input("Escolha a busca: ")

		if search_type == "1":
			busca = "Profundidade"
			way = search.dfs(file_input).way()
			total = search.dfs(file_input).total()
		elif search_type == "2":
			busca = "Gulosa"
			way = search.greedy(file_input).way()
			total = search.greedy(file_input).total()
		else:
			menu = False
			way = None
		

		if way is not None:
			print("\nBusca " + busca)
			print("\n")
			print("Caminho percorrido: ")
			print(way)
			print("Custo total: " + str(total))
			input()
			
			try:
				os.system('cls')	
				os.system('clear')	
			except:
				pass
			
			print("Caminho do arquivo: "+file_to_read)


if __name__=="__main__":
	main()
